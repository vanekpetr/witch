<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooleanResourcesTable extends Migration //BooleanResource can be used for example for "display this?" yes/no
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boolean_resources', function (Blueprint $table) {
            $table->id();

            $table->string('name')->unique();
            $table->boolean('content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boolean_resources');
    }
}
