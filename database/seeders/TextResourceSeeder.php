<?php

namespace Database\Seeders;

use App\Models\TextResource;
use Illuminate\Database\Seeder;

class TextResourceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bio = new TextResource();
        $bio->name = 'bio';
        $bio->content = 'sample bio text - edit this';
        $bio->save();
    }
}
