<?php

namespace Database\Seeders;

use App\Models\ImageResource;
use Illuminate\Database\Seeder;

class ImageResourceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bio = new ImageResource();
        $bio->name = 'bio';
        $bio->path = '/assets/witch-no-media.png';
        $bio->save();
    }
}
