<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = new Page();
        $home->name = 'home';
        $home->display_name = 'domů';
        $home->route = '/';
        $home->view = 'home';
        $home->save();

        $portfolio = new Page();
        $portfolio->name = 'portfolio';
        $portfolio->display_name = 'portfolio';
        $portfolio->route = '/portfolio';
        $portfolio->view = 'portfolio';
        $portfolio->save();

        $services = new Page();
        $services->name = 'services';
        $services->display_name = 'služby';
        $services->route = '/sluzby';
        $services->view = 'services';
        $services->save();

        $bio = new Page();
        $bio->name = 'bio';
        $bio->display_name = 'bio';
        $bio->route = '/bio';
        $bio->view = 'bio';
        $bio->save();
    }
}
