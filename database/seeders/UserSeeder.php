<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->username = "admin";
        $admin->email = 'admin.admin@admin.admin';
        $admin->password = bcrypt("admin");
        $admin->save();
    }
}
