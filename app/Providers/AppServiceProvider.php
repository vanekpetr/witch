<?php

namespace App\Providers;

use App\Http\Controllers\BuildableController;
use App\Http\Controllers\MasterUserController;
use App\Models\Anchor;
use App\Models\Article;
use App\Models\BooleanResource;
use App\Models\Collection;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Heading;
use App\Models\ImageResource;
use App\Models\Page;
use App\Models\Photo;
use App\Models\Service;
use App\Models\Tag;
use App\Models\TextResource;
use App\Models\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() //bota
    {
        View::composer('*', function($view)
        {
            $master_username = MasterUserController::$master_username;
            $view->with('master_username', $master_username);

            $users = User::query()->get();
            $view->with('users', $users);

            $pages = Page::query()->get();
            $view->with('pages', $pages);

            $collections = Collection::query()->get();
            $view->with('collections', $collections);

            $photos = Photo::query()->get();
            $view->with('photos', $photos);

            $articles = Article::query()->get()->sortBy('position');
            $view->with('articles', $articles);

            $tags = Tag::query()->get();
            $view->with('tags', $tags);

            $anchors = Anchor::query()->get()->sortBy('position');
            $view->with('anchors', $anchors);

            $galleries = Gallery::query()->get();
            $view->with('galleries', $galleries);

            $headings = Heading::query()->get();
            $view->with('headings', $headings);

            $contacts = Contact::query()->get();
            $view->with('contacts', $contacts);

            $services = Service::query()->get()->sortBy('position');
            $view->with('services', $services);

            $text_resources = TextResource::query()->get();
            $view->with('text_resources', $text_resources);

            $text_resource_class = TextResource::class;
            $view->with('text_resource_class', $text_resource_class);

            $image_resource_class = ImageResource::class;
            $view->with('image_resource_class', $image_resource_class);

            $boolean_resource_class = BooleanResource::class;
            $view->with('boolean_resource_class', $boolean_resource_class);

            $buildable_controller_class = BuildableController::class;
            $view->with('buildable_controller_class', $buildable_controller_class);
        });
    }
}
