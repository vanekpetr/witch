<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    protected $guarded = []; //turn of guarding [!]

    public function collection() {
        return $this->belongsTo(Collection::class, 'collection_id', 'id');
    }
}
