<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anchor extends Model
{
    use HasFactory;

    public function page() {
        return $this->belongsTo(Page::class);
    }

    protected $fillable = [
        'name',
        'position',
        'page_name'
    ];

    protected $primaryKey = 'name';
    public $incrementing = false;
}
