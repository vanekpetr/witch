<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function articles() {
        return $this->hasMany(Article::class);
    }

    public function anchors() {
        return $this->hasMany(Anchor::class);
    }

    public function galleries() {
        return $this->hasMany(Gallery::class);
    }

    public function headings() {
        return $this->hasMany(Heading::class);
    }

    protected $fillable = [
        'name',
        'display_name',
        'route',
        'view',
    ];

    protected $primaryKey = 'name';
    public $incrementing = false;
}
