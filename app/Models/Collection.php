<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    protected $guarded = []; //turn of guarding [!]

    public function photos() {
        return $this->hasMany(Photo::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class, 'collection_tag', 'collection_id', 'tag_id');
    }

    protected $fillable = [
        'id',
        'url',
        'name',
        'description',
        'view',
    ];
}
