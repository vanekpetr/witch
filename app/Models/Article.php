<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function page() {
        return $this->belongsTo(Page::class, 'page_name', 'name');
    }

    protected $fillable = [
        'id',
        'title',
        'subtitle',
        'text',
        'page_name',
        'position'
    ];
}
