<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageResource extends Model
{
    use HasFactory;

    public static function get_image($name) {
        if (ImageResource::query()->where('name', '=', $name)->first() != null) {
            return ImageResource::query()->where('name', '=', $name)->first()->path;
        } else {
            return null;
        }
    }

    public static function get_resource($name) {
        return ImageResource::query()->where('name', '=', $name)->first();
    }

    public static function get_group($group) {
        return ImageResource::query()->where('group', '=', $group)->get();
    }

    protected $fillable = [
        'name',
        'group',
        'path'
    ];
}
