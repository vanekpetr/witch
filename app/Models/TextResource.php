<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TextResource extends Model
{
    use HasFactory;

    public static function get_text($name) {
        return TextResource::query()->where('name', '=', $name)->first()->content;
    }

    public static function get_resource($name) {
        return TextResource::query()->where('name', '=', $name)->first();
    }
}
