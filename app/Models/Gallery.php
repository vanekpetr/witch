<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    public function page() {
        return $this->belongsTo(Page::class, 'page_name', 'name');
    }

    protected $fillable = [
        'page_name',
        'position'
    ];
}
