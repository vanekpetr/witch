<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BooleanResource extends Model //can be used for example for "display this?" yes/no
{
    use HasFactory;

    public static function get_text($name) {
        return BooleanResource::query()->where('name', '=', $name)->first()->content;
    }

    public static function get_resource($name) {
        return BooleanResource::query()->where('name', '=', $name)->first();
    }
}
