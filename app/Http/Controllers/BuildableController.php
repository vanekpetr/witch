<?php

namespace App\Http\Controllers;

use App\Models\Anchor;
use App\Models\Article;
use App\Models\Gallery;
use App\Models\Heading;

class BuildableController extends AdminController
{
    public static function increment($page_name, $position, $amount = 1) {
        $incrementable_articles = Article::query()->where('page_name', '=', $page_name)->where('position', '>=', $position);
        $incrementable_articles->increment('position', $amount);

        $incrementable_anchors = Anchor::query()->where('page_name', '=', $page_name)->where('position', '>=', $position);
        $incrementable_anchors->increment('position', $amount);

        $incrementable_galleries = Gallery::query()->where('page_name', '=', $page_name)->where('position', '>=', $position);
        $incrementable_galleries->increment('position', $amount);

        $incrementable_headings = Heading::query()->where('page_name', '=', $page_name)->where('position', '>=', $position);
        $incrementable_headings->increment('position', $amount);
    }

    public static function move_buildable_element($buildable_element, $page_name, $from, $to) {
        $buildable_element->position = -1;
        $buildable_element->save();

        BuildableController::increment($page_name, $from, -1);
        if ($to >= $from) {
            $to--;
        }
        BuildableController::increment($page_name, $to, 1);

        $buildable_element->position = $to;
        $buildable_element->save();
    }

    public static function get_all($page_name) {
        $elements =
            Article::query()->where('page_name', '=', $page_name)->get();
        $elements = $elements->merge(
            Anchor::query()->where('page_name', '=', $page_name)->get()
        );
        $elements = $elements->merge(
            Heading::query()->where('page_name', '=', $page_name)->get()
        );
        $elements = $elements->merge(
            Gallery::query()->where('page_name', '=', $page_name)->get()
        );
        return $elements->sortBy('position');
    }
}
