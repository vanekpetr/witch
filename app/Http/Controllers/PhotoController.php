<?php

namespace App\Http\Controllers;

use App\Models\Collection;
use App\Models\Photo;
use Illuminate\Support\Facades\File;

class PhotoController extends AdminController
{
    public function store($collection_url) {
        $photo = request()->file;

        //$photo_name = $photo->store('photos', 'public'); //<--- does not work on hosting (needs storage link)
        //Storage::disk('photos')->put('', $photo); //<--- this also works
        $photo_name = $photo->store('', ['disk' => 'photos']);

        $data = [
            'path' => '/photos/'.$photo_name,
            'collection_id' => Collection::query()->where('url', '=', $collection_url)->first()->id
        ];

        $photo = Photo::create($data);

        return response()->json(['file' => $photo->path]);
    }

    public function delete() {
        $photo = Photo::query()->where('id', '=', request()->id)->first();
        $collection_url = $photo->collection()->first()->url;

        File::delete(public_path().$photo->path);
        $photo->delete();

        return redirect('/admin/collection/edit/'.$collection_url);
    }
}
