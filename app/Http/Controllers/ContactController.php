<?php

namespace App\Http\Controllers;

use App\Models\Contact;

class ContactController extends AdminController {
    public function edit() {
        return view('overlays.edit-contact');
    }

    public function store() {
        $link_text = request()->link_text;
        if (request()->link_text == '') {
            $link_text = request()->link;
        }

        $data = [
            'text' => request()->text,
            'type' => request()->type,
            'link_text' => $link_text,
            'link' => request()->link,
        ];

        Contact::create($data);

        return redirect('/edit/contact');
    }

    public function update() {
        $contact = Contact::query()->where('id', '=', request()->id)->first();

        $link_text = request()->link_text;
        if (request()->link_text == '') {
            $link_text = request()->link;
        }

        $contact->text = request()->text;
        $contact->type = request()->type;
        $contact->link_text = $link_text;
        $contact->link = request()->link;

        $contact->save();

        return redirect('/edit/contact');
    }

    public function confirm_delete() {
        return view('overlays.are-you-sure', [
            'action' => '/edit/contact/delete?id='.request()->id,
            'href' => '/edit/contact',
            'page_view' => 'overlays.edit-contact',
            'title' => 'delete contact?'
        ]);
    }

    public function delete() {
        Contact::query()->where('id', '=', request()->id)->first()->delete();

        return redirect('/edit/contact');
    }
}
