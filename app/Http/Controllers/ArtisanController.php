<?php //stolen from https://www.kutac.cz/weby-a-vse-okolo/laravel-na-sdilenem-hostingu-wedos#zprovozneni-artisancall

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\StreamOutput;

class ArtisanController extends Controller
{
    private $previousErrorHandler = false;

    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        if ($errno == 2 && preg_match('/putenv\(\)/i', $errstr)) {
            return true;
        }
        if ($this->previousErrorHandler != null) {
            return call_user_func($this->previousErrorHandler, $errno, $errstr, $errfile, $errline);
        }
        return false;
    }

    protected function callArtisan($command, $parameters = [])
    {
        if ($this->previousErrorHandler === false) {
            $this->previousErrorHandler = set_error_handler( [$this, 'errorHandler']);
        }

        $outputStream = fopen('php://output', 'w');
        $output = new StreamOutput($outputStream);

        ob_start();
        echo "Command: $command\n---------\n";
        $exit = Artisan::call($command, $parameters, $output);
        echo "Exit code: ".intval($exit);

        $outputText = ob_get_clean();
        fclose($outputStream);

        return response($outputText, 200)
            ->header('Content-Type', 'text/plain');
    }

    // Routy jsou nasměrovány na tuto funkci a podobné
    public function refresh()
    {
        $this->callArtisan("cache:clear");
        //$this->callArtisan("route:cache");
        $this->callArtisan("view:cache");
        //$this->callArtisan("key:generate --force");
        $this->callArtisan("config:cache");

        return view('admin-pages.message');
    }
}
