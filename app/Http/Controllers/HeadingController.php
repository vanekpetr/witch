<?php


namespace App\Http\Controllers;


use App\Models\Heading;
use App\Models\Page;

class HeadingController extends BuildableController
{
    public function create() {
        return view('overlays.create-heading', [
            'page' => Page::query()->where('name', '=', request()->page_name)->first()
        ]);
    }

    public function store() {
        $page_name = request()->page_name;
        $page = Page::query()->where('name', '=', $page_name)->first();
        $position = request()->position;

        $data = [
            'title' => request()->title,
            'subtitle' => request()->subtitle,
            'page_name' => $page_name,
            'position' => $position
        ];

        BuildableController::increment($page_name, $position, 1);

        $created = Heading::create($data);

        return redirect($page->route.'#heading'.$created->id);
    }

    public function edit() {
        $heading = Heading::query()->where('id', '=', request()->id)->first();
        $page = $heading->page()->first();

        return view('overlays.edit-heading', [
            'heading' => $heading,
            'page' => $page
        ]);
    }

    public function update() {
        $heading = Heading::query()->where('id', '=', request()->id)->first();
        $page = $heading->page()->first();

        $heading->title = request()->title;
        $heading->subtitle = request()->subtitle;

        $heading->save();

        return redirect($page->route.'#heading'.request()->id);
    }

    public function confirm_delete() {
        $heading = Heading::query()->where('id', '=', request()->id)->first();
        $page = $heading->page()->first();

        return view('overlays.are-you-sure', [
            'action' => '/edit/heading/delete?id='.request()->id,
            'href' => $page->route.'#heading'.request()->id,
            'page_view' => 'pages.'.$page->view,
            'title' => 'delete heading?',
            'text' => $heading->title.' '.$heading->subtitle
        ]);
    }

    public function delete() {
        $heading = Heading::query()->where('id', '=', request()->id)->first();
        $page = $heading->page()->first();

        $heading->delete();

        BuildableController::increment($page->name, $heading->position, -1);

        return redirect($page->route);
    }

    public function move() {
        $heading = Heading::query()->where('page_name', '=', request()->page_name)->where('position', '=', request()->from)->first();

        BuildableController::move_buildable_element($heading, request()->page_name, request()->from, request()->to);

        return redirect($heading->page()->first()->route.'#heading'.$heading->id);
    }
}
