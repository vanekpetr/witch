<?php

namespace App\Http\Controllers;

use App\Models\Anchor;
use App\Models\Page;

class AnchorController extends BuildableController
{
    public function create() {
        return view('overlays.create-anchor', [
            'page' => Page::query()->where('name', '=', request()->page_name)->first()
        ]);
    }

    public function store() {
        $page_name = request()->page_name;
        Log:info("page name: ".$page_name);
        $page = Page::query()->where('name', '=', $page_name)->first();
        $position = request()->position;
        $data = [
            'name' => request()->name,
            'page_name' => $page_name,
            'position' => $position
        ];

        BuildableController::increment($page_name, $position);

        $created = Anchor::create($data);

        return redirect($page->route.'#'.$created->name);
    }

    public function confirm_delete() {
        $anchor = Anchor::query()->where('name', '=', request()->name)->first();
        $page = $anchor->page()->first();

        return view('overlays.are-you-sure', [
            'action' => '/edit/anchor/delete?name='.request()->name,
            'href' => $page.'#'.request()->name,
            'page_view' => 'pages.'.$page->view,
            'title' => 'delete anchor?',
            'text' => '#'.$anchor->name
        ]);
    }

    public function delete() {
        $anchor = Anchor::query()->where('name', '=', request()->name)->first();
        $page = $anchor->page()->first();

        $anchor->delete();

        BuildableController::increment($page->name, $anchor->position, -1);

        return redirect($page->route);
    }

    public function move() {
        $anchor = Anchor::query()->where('page_name', '=', request()->page_name)->where('position', '=', request()->from)->first();

        BuildableController::move_buildable_element($anchor, request()->page_name, request()->from, request()->to);

        return redirect($anchor->page()->first()->route.'#'.$anchor->name);
    }
}
