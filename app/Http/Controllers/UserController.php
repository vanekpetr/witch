<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends AdminController
{
    public function edit() {
        $user = User::query()->where('username', '=', request()->username)->first();
        $master_user = User::query()->where('username', '=', MasterUserController::$master_username)->first();
        if (Auth::id() == $user->id || Auth::id() == $master_user->id) {
            return view('overlays.edit-user');
        }
    }

    public function update() {
        $user = User::query()->where('username', '=', request()->username)->first();
        $master_user = User::query()->where('username', '=', MasterUserController::$master_username)->first();
        $this->validate(request(), [
            'password' => 'required|confirmed'
        ]);
        if (Auth::id() == $user->id || Auth::id() == $master_user->id) {
            $user->password = bcrypt(request()->password);
            $user->save();
        }

        return redirect('/admin/users');
    }
}
