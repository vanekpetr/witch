<?php


namespace App\Http\Controllers;


use App\Models\Gallery;
use App\Models\ImageResource;
use App\Models\Page;
use Illuminate\Support\Facades\File;

class GalleryController extends BuildableController
{
    public function create() {
        $page_name = request()->page_name;
        $page = Page::query()->where('name', '=', $page_name)->first();
        $position = request()->position;

        $data = [
            'page_name' => $page_name,
            'position' => $position
        ];

        BuildableController::increment($page_name, $position, 1);

        $created = Gallery::create($data);

        ImageResource::create([
            'name' => 'gallery'.$created->id.':image1',
            'group' => 'gallery'.$created->id,
            'path' => '/assets/witch-no-media.png'
        ]);

        return redirect($page->route.'#gallery'.$created->id);
    }

    /*public function edit() {

        $gallery = Gallery::query()->where('id', '=', request()->id)->first();
        $resource_group = ImageResource::get_group('gallery'.$gallery->id);
        $page = Page::query()->where('name', '=', $gallery->page_name)->first();

        return view('overlays.edit-image-resource-group', [
            'view' => 'pages.'.$page->view,
            'route' => $page->route,
            'anchor' => 'gallery'.$gallery->id,
            'resource_group' => $resource_group,
            'resource' => $resource = ImageResource::get_resource(request()->resource) ?? $resource_group->first(),
            'url' => '/edit/gallery',
            'url_variables' => 'id='.request()->id
        ]);
    }*/

    public function confirm_delete() {
        $gallery = Gallery::query()->where('id', '=', request()->id)->first();
        $page = $gallery->page()->first();

        return view('overlays.are-you-sure', [
            'action' => '/edit/gallery/delete?id='.request()->id,
            'href' => $page->route.'#gallery'.request()->id,
            'page_view' => 'pages.'.$page->view,
            'title' => 'delete gallery?',
            'text' => 'gallery#'.$gallery->id
        ]);
    }

    public function delete() {
        $gallery = Gallery::query()->where('id', '=', request()->id)->first();
        $page = $gallery->page()->first();

        foreach(ImageResource::query()->where('group', '=', 'gallery'.$gallery->id)->get() as $image_resource) {
            File::delete(public_path().$image_resource->path);
            $image_resource->delete();
        }
//        ImageResource::query()->where('group', '=', 'gallery'.$gallery->id)->delete();
        $gallery->delete();

        BuildableController::increment($page->name, $gallery->position, -1);

        return redirect($page->route);
    }

    public function move() {
        $gallery = Gallery::query()->where('page_name', '=', request()->page_name)->where('position', '=', request()->from)->first();

        BuildableController::move_buildable_element($gallery, request()->page_name, request()->from, request()->to);

        return redirect($gallery->page()->first()->route.'#gallery'.$gallery->id);
    }
}
