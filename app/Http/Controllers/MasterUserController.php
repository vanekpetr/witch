<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class MasterUserController extends AdminController
{
    public static $master_username = 'admin';

    public function create() {
        $master_user = User::query()->where('username', '=', $this::$master_username)->first();
        if (Auth::id() == $master_user->id) {
            return view('overlays.create-user');
        }
    }

    public function store() {
        $master_user = User::query()->where('username', '=', $this::$master_username)->first();
        if (Auth::id() == $master_user->id) {
            $data = request()->validate([ //stolen from RegisterController::validator()
                'username' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $user = new User();
            $user->username = $data['username'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->save();
        }

        return redirect('/admin/users');
    }

    public function confirm_delete() {
        $master_user = User::query()->where('username', '=', $this::$master_username)->first();
        if (Auth::id() == $master_user->id && request()->username != $master_user->username) {
            return view('overlays.are-you-sure', [
                'action' => '/admin/users/delete?username='.request()->username,
                'href' => '/admin/users',
                'page_view' => 'overlays.users',
                'title' => 'delete user?',
                'text' => request()->username
            ]);
        } else {
            return redirect('/admin/users');
        }
    }

    public function delete() {
        $master_user = User::query()->where('username', '=', $this::$master_username)->first();
        $user = User::query()->where('username', '=', request()->username)->first();
        if (Auth::id() == $master_user->id && request()->username != $master_user->username) {
            $user->delete();
        }
        return redirect('/admin/users');
    }
}
