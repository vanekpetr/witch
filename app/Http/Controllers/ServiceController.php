<?php


namespace App\Http\Controllers;


use App\Models\ImageResource;
use App\Models\Page;
use App\Models\Service;
use Illuminate\Support\Facades\File;

class ServiceController extends AdminController
{
    public static function increment($position ,$amount) {
        $incrementable = Service::query()->where('position', '>=', $position);
        $incrementable->increment('position', $amount);
    }

    public static function move_service($service, $from, $to) {
        $service->position = -1;
        $service->save();

        ServiceController::increment($from, -1);
        if ($to >= $from) {
            $to--;
        }
        ServiceController::increment($to, 1);

        $service->position = $to;
        $service->save();
    }

    public function create() {
        return view('overlays.create-service');
    }

    public function store() {
        $data = [
            'title' => request()->title,
            'pricing' => request()->pricing,
            'description' => request()->description,
            'position' => request()->position
        ];

        ServiceController::increment(request()->position, 1);

        $created = Service::create($data);

        return redirect('/sluzby#service'.$created->id);
    }

    public function edit() {
        $service = Service::query()->where('id', '=', request()->id)->first();
        return view('overlays.edit-service', [ 'service' => $service ]);
    }

    public function update() {
        $service = Service::query()->where('id', '=', request()->id)->first();

        $service->title = request()->title;
        $service->pricing = request()->pricing;
        $service->description = request()->description;

        $service->save();

        return redirect('/sluzby#service'.request()->id);
    }

    public function edit_title_image() {

        $page = Page::query()->where('name', '=', 'services')->first();

        return view('overlays.edit-image-resource', [
            'resource' => ImageResource::query()->where('name', '=', 'service'.request()->id)->first(),
            'page' => $page,
            'anchor' => request()->anchor ?? '',
            'title' => 'edit service'.request()->id.' title image'
        ]);
    }

    public function add_title_image() {
        ImageResource::create([
            'name' => 'service'.request()->id,
            'path' => '/assets/witch-no-media.png',
            'group' => 'service'.request()->id
        ]);

        return redirect('/edit/service/image?id='.request()->id.'&page=service&anchor=service'.request()->id);
    }

    public function remove_title_image() {
        $image_resource = ImageResource::query()->where('name', '=', 'service'.request()->id)->first();
        $image_resource->delete();

        return redirect('/sluzby#service'.request()->id);
    }

    public function edit_sample_images() {

        $resource_group = ImageResource::get_group('service'.request()->id);
//        $resource = ImageResource::get_resource(request()->resource) ?? $resource_group->first();
        $page = Page::query()->where('name', '=', 'services')->first();

        return redirect('/edit/image/g?name=service'.request()->id.'&page='.$page->name.'&anchor=service'.request()->id);

        /*return view('overlays.edit-image-resource-group', [
            'page' => $page,
            'anchor' => '',
            'resource_group' => $resource_group,
            'resource' => $resource,
            'url' => '/edit/service/sample_images',
            'url_variables' => 'id='.request()->id
        ]);*/
    }

    public function confirm_delete() {

        $service = Service::query()->where('id', '=', request()->id)->first();

        return view('overlays.are-you-sure', [
            'action' => '/edit/service/delete?id='.request()->id,
            'href' => '/sluzby#service'.request()->id,
            'page_view' => 'pages.services',
            'title' => 'delete service?',
            'text' => $service->title
        ]);
    }

    public function delete() {
        $service = Service::query()->where('id', '=', request()->id)->first();

        foreach(ImageResource::query()->where('group', '=', 'service'.$service->id)->get() as $image_resource) {
            File::delete(public_path().$image_resource->path);
            $image_resource->delete();
        }
        $service->delete();

        ServiceController::increment($service->position, -1);

        return redirect('/sluzby');
    }

    public function move() {
        $service = Service::query()->where('position', '=', request()->from)->first();

        ServiceController::move_service($service, request()->from, request()->to);

        return redirect('/sluzby#service'.$service->id);
    }
}
