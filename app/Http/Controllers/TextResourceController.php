<?php


namespace App\Http\Controllers;


use App\Models\TextResource;

class TextResourceController extends Controller
{
    public function edit() {
        return view('overlays.edit-text-resource', [
            'resource' => TextResource::query()->where('name', '=', request()->name)->first(),
            'route' => request()->route,
            'view' => request()->view,
            'anchor' => request()->anchor ?? '',
            'title' => request()->title,
            'text' => request()->text
        ]);
    }

    public function update() {
        $text_resource = TextResource::query()->where('name', '=', request()->name)->first();

        $text_resource->content = request()->resource_content;

        $text_resource->save();

        return redirect(request()->href);
    }
}
