<?php


namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Page;

class ArticleController extends BuildableController
{
    public function create() {
        return view('overlays.create-article', [
            'page' => Page::query()->where('name', '=', request()->page_name)->first()
        ]);
    }

    public function store() {
        $page_name = request()->page_name;
        $page = Page::query()->where('name', '=', $page_name)->first();
        $position = request()->position;
        $data = [
            'title' => request()->title,
            'subtitle' => request()->subtitle,
            'text' => request()->text,
            'page_name' => $page_name,
            'position' => $position
        ];

        BuildableController::increment($page_name, $position, 1);

        $created = Article::create($data);

        return redirect($page->route.'#article'.$created->id);
    }

    public function edit() {
        $article = Article::query()->where('id', '=', request()->id)->first();
        $page = $article->page()->first();

        //Log:info($page);

        return view('overlays.edit-article', [
            'page' => $page,
            'article' => $article
        ]);

        /*return view('admin-elements.popup-menu', [
            'page_view' => 'pages.'.$page->view,
            'href' => $page->route,
            'options' => [
                'option 1' => 'href 1',
                'option 2' => 'href 2',
                'option 3' => 'href 3'
            ]
        ]);*/
    }

    public function update() {
        $article = Article::query()->where('id', '=', request()->id)->first();
        $page = $article->page()->first();

        $article->title = request()->title;
        $article->subtitle = request()->subtitle;
        $article->text = request()->text;

        $article->save();

        return redirect($page->route.'#article'.request()->id);
    }

    public function confirm_delete() {
        $article = Article::query()->where('id', '=', request()->id)->first();
        $page = $article->page()->first();

        return view('overlays.are-you-sure', [
            'action' => '/edit/article/delete?id='.request()->id,
            'href' => $page->route.'#article'.request()->id,
            'page_view' => 'pages.'.$page->view,
            'title' => 'delete article?',
            'text' => $article->title.' '.$article->subtitle
        ]);
    }

    public function delete() {
        $article = Article::query()->where('id', '=', request()->id)->first();
        $page = $article->page()->first();

        $article->delete();

        BuildableController::increment($page->name, $article->position, -1);

        return redirect($page->route);
    }

    public function move() {
        $article = Article::query()->where('page_name', '=', request()->page_name)->where('position', '=', request()->from)->first();

        BuildableController::move_buildable_element($article, request()->page_name, request()->from, request()->to);

        return redirect($article->page()->first()->route.'#article'.$article->id);
    }
}
