<?php

namespace App\Http\Controllers;

use App\Models\Page;

class WebController extends Controller
{
    public function index($page_name = '')
    {
        $page = Page::query()->where('route', '=', '/'.$page_name)->first();
        if ($page == null) { //implicit redirect to home
            return redirect('/');
        }
        return view('pages.'.$page['view']);
    }
}
