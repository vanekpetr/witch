<?php


namespace App\Http\Controllers;



use App\Models\ImageResource;
use App\Models\Page;
use App\Models\Photo;
use Illuminate\Support\Facades\File;

class ImageResourceController extends AdminController
{
    public function create() { //should NOT be used for images without a group
        $resource_name = request()->group.':image'.( ImageResource::get_group(request()->group)->count()+1 );
        $data = [
            'name' => $resource_name,
            'group' => request()->group,
            'path' => '/assets/witch-no-media.png'
        ];

        ImageResource::create($data);

        return redirect('/edit/image/g?page='.request()->page.'&name='.request()->group.'&resource='.$resource_name.'&anchor='.request()->anchor.'#'.request()->anchor);
    }

    public function edit() {

        $page = Page::query()->where('name', '=', request()->page)->first();

        return view('overlays.edit-image-resource', [
            'resource' => ImageResource::query()->where('name', '=', request()->name)->first(),
            'anchor' => request()->anchor ?? '',
            'page' => $page
            /*,
            'title' => request()->title,
            'text' => request()->text*/
        ]);
    }

    public function edit_group() {
        $page = Page::query()->where('name', '=', request()->page)->first();
        $resource_group = ImageResource::get_group(request()->name);
        $resource = ImageResource::get_resource(request()->resource) ?? $resource_group->first();

        return view('overlays.edit-image-resource-group', [
            'page' => $page,
            'anchor' => request()->anchor ?? '',
            'resource_group' => $resource_group,
            'resource' => $resource,
        ]);
    }

    public function update() {
        $page = Page::query()->where('name', '=', request()->page)->first();

        $image_resource = ImageResource::query()->where('name', '=', request()->name)->first();

        if (!str_starts_with($image_resource->path, '/photos/') && !str_starts_with($image_resource->path, '/assets/')) {
            File::delete(public_path().$image_resource->path);
        }

        $file = request()->file;
        /*$file_name = request()->name.'.'.$file->getClientOriginalExtension();
        $file->storeAs('image-resources', $file_name, 'public');*/
        $file_name = request()->name.'.'.$file->getClientOriginalExtension();
        $file->storeAs('', $file_name, ['disk' => 'image-resources']);

        $image_resource->path = '/image-resources/'.$file_name;
        $image_resource->save();

        if($image_resource->group != null) {
            return redirect('/edit/image/g?name='.$image_resource->group.'&resource='.request()->name.'&page='.request()->page.'&anchor='.request()->anchor.'#'.request()->anchor);
        } else {
            return redirect($page->route.'#'.request()->anchor);
        }
    }

    public function link() {
        $page = Page::query()->where('name', '=', request()->page)->first();

        $image_resource = ImageResource::query()->where('name', '=', request()->name)->first();

        $photo_path = Photo::query()->where('id', '=', request()->photo_id)->first()->path;

        //Log::info('/storage/image-resources/'.request()->name.'.'.pathinfo($photo_path)['extension']);

        if(request()->create_copy) {
            if (!str_starts_with($image_resource->path, '/photos/') && !str_starts_with($image_resource->path, '/assets/')) {
                File::delete(public_path().$image_resource->path);
            }

            $image_resource->path = '/image-resources/'.request()->name.'.'.pathinfo($photo_path)['extension'];
            $image_resource->save();

            File::copy( //for creating a copy of a file from photos
                public_path().$photo_path,
                public_path().'/image-resources/'.request()->name.'.'.pathinfo($photo_path)['extension']
            );
        } else {
            if (!str_starts_with($image_resource->path, '/photos/') && !str_starts_with($image_resource->path, '/assets/')) {
                File::delete(public_path().$image_resource->path);
            }

            $image_resource->path = $photo_path;
            $image_resource->save();
        }

        if($image_resource->group != null) {
            return redirect('/edit/image/g?name='.$image_resource->group.'&resource='.request()->name.'&page='.request()->page.'&anchor='.request()->anchor.'#'.request()->anchor);
        } else {
            return redirect($page->route.'#'.request()->anchor);
        }
    }

    public function confirm_delete() {

        $page = Page::query()->where('name', '=', request()->page)->first();
        $group_name = ImageResource::get_resource(request()->name)->group;

        return view('overlays.are-you-sure', [
            'action' => '/edit/image/g/delete?name='.request()->name.'&page='.request()->page.'&anchor='.request()->anchor,
            'href' => '/edit/image/g?name='.$group_name.'&page='.request()->page.'&anchor='.request()->anchor,
            'page_view' => 'pages.'.$page->view,
            'title' => 'delete '.request()->name.'?'
        ]);
    }

    public function delete() {
        $image_resource = ImageResource::get_resource(request()->name);
        $group_name = $image_resource->group;

        File::delete(public_path().$image_resource->path);
        $image_resource->delete();

        return redirect('/edit/image/g?name='.$group_name.'&page='.request()->page.'&anchor='.request()->anchor);
    }
}
