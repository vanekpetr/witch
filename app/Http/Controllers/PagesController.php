<?php

namespace App\Http\Controllers;

use App\Models\Tag;

class PagesController extends WebController
{
    public function __construct()
    {
        //TODO: add redirect later
    }

    public function tag($tag_name) {
        $tag = Tag::query()->where('name', '=', $tag_name)->first();

        return view('pages.portfolio', [
            'show_tag' => $tag,
            'selected_collections' => $tag->collections()->get()
        ]);
    }
}
