<?php


namespace App\Http\Controllers;


use App\Models\Collection;
use App\Models\ImageResource;
use App\Models\Photo;

class ViewerController extends WebController {
    /*public function collection($collection_url) {
        $collection_id = Collection::query()->where('url', '=', $collection_url)->first()->id;
        return redirect(
            '/photo/'.Photo::query()->where('collection_id', '=', $collection_id)->first()->id
        );
    }*/

    /*public function photo($photo_id) {
        $photo = Photo::query()->where('id', '=', $photo_id)->first();
        $collection = $photo->collection()->first();
        $previous_id = Photo::query()->where('id', '<', $photo->id)->max('id');
        $next_id = Photo::query()->where('id', '>', $photo->id)->min('id');
        return view('viewer', [
            'collection' => $photo->collection()->first(),
            'photo_number' => $photo->id,
            'total_photos' => Photo::query()->where('collection_id', '=', $collection->id)->count(),
            'path' => "/storage/".$photo->path,
            'previous_id' => $previous_id,
            'next_id' => $next_id,
            'from' => request()->from ?? '/portfolio'
        ]);
        //viewer parameters: collection, photo_number, total_photos, path, previous_path, next_path
    }*/

    public function group($name) {

        $id = request()->id ?? ImageResource::get_group($name)->first()->id;
        $path = ImageResource::query()->where('id', '=', $id)->first()->path ?? '/storage/web/witch-image-missing.png';
        $from = request()->from ?? '/';

        $previous_id =
            ImageResource::query()->where('id', '<', $id)->where('group', '=', $name)->max('id')
            ??
            ImageResource::query()->where('group', '=', $name)->max('id');
        $next_id =
            ImageResource::query()->where('id', '>', $id)->where('group', '=', $name)->min('id');

        return view('viewer', [
            'title' => request()->title ?? 'preview',
            'path' => $path,
            'from' => $from,
            'next' => '/view/g/'.$name.'?id='.$next_id.'&from='.$from.'&title='.request()->title ?? 'preview',
            'previous' => '/view/g/'.$name.'?id='.$previous_id.'&from='.$from.'&title='.request()->title ?? 'preview',
            'counter' => ImageResource::get_group($name)->where('id', '<=', $id)->count().'/'.ImageResource::get_group($name)->count()
        ]);
    }

    public function collection($url) {
        $collection = Collection::query()->where('url', '=', $url)->first();
        $id = request()->id ?? $collection->photos()->first()->id;
        $path = Photo::query()->where('id', '=', $id)->first()->path ?? 'web/witch-image-missing.png';
        $from = request()->from ?? '/portfolio';
        $previous_id =
            $collection->photos()->where('id', '<', $id)->max('id')
            ??
            $collection->photos()->max('id');
        $next_id = $collection->photos()->where('id', '>', $id)->min('id');

        return view('viewer', [
            'title' => $collection->name,
            'title_href' => '/portfolio#'.$collection->url,
            'subtitle_tags' => $collection->tags()->get(),
            'path' => $path,
            'from' => $from,
            'previous' => '/view/'.$url.'?id='.$previous_id.'&from='.$from,
            'next' => '/view/'.$url.'?id='.$next_id.'&from='.$from,
            'counter' => $collection->photos()->where('id', '<=', $id)->count().'/'.$collection->photos()->count()
        ]);
    }
}
