<?php


namespace App\Http\Controllers;


use App\Models\ImageResource;
use App\Models\Page;
use App\Models\TextResource;

class BioController extends Controller
{

    public function edit_text() {
        return view('overlays.edit-text-resource', [
            'resource' => TextResource::query()->where('name', '=', 'bio')->first(),
            'route' => '/bio',
            'view' => 'pages.bio',
            'anchor' => '',
            'title' => 'edit bio'
        ]);
    }

    public function edit_image() {
        return view('overlays.edit-image-resource', [
            'resource' => ImageResource::query()->where('name', '=', 'bio')->first(),
            'route' => '/bio',
            'view' => 'pages.bio',
            'anchor' => '',
            'title' => 'edit bio image',
            'page' => Page::query()->where('name', '=', 'bio')->first()
        ]);
    }
}
