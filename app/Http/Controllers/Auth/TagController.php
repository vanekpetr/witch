<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\AdminController;
use App\Models\Collection;
use App\Models\Tag;

class TagController extends AdminController
{
    public function create() {
        return view('overlays.create-tag');
    }

    public function store() {
        $data = request()->validate([
            'name' => 'required|unique:tags'
        ]);

        Tag::create($data);

        return redirect('/admin');
    }

    public function edit() {
        return view('overlays.edit-tag', [ 'tag' => Tag::query()->where('name', '=', request()->name)->first() ]);
    }

    public function update() {
        $tag = Tag::query()->where('name', '=', request()->original_name)->first();

        $tag->name = request()->name;

        $tag->save();

        return redirect('/admin/tag/edit?name='.$tag->name);
    }

    public function add($collection_url) {
        $collection = Collection::query()->where('url', '=', $collection_url)->first();
        $tag = Tag::query()->where('name', '=', request()->name)->first();

        if($tag == null) {
            $data = request()->validate([
                'name' => 'required|unique:tags'
            ]);

            $tag = Tag::create($data);
        }

        $collection->tags()->attach($tag);

        return redirect('/admin/collection/edit/'.$collection->url);
    }

    public function remove($collection_url) {
        $collection = Collection::query()->where('url', '=', $collection_url)->first();
        $tag = Tag::query()->where('name', '=', request()->name)->first();

        $tag->collections()->detach($collection);

        return redirect('/admin/collection/edit/'.$collection->url);
    }

    public function remove_collection() {
        $collection = Collection::query()->where('url', '=', request()->collection_url)->first();
        $tag = Tag::query()->where('name', '=', request()->name)->first();

        $tag->collections()->detach($collection);

        return redirect('/admin/tag/edit?name='.$tag->name);
    }

    public function confirm_delete() {
        $tag = Tag::query()->where('name', '=', request()->name)->first();

        $collections_string = '';

        foreach($tag->collections()->get() as $collection) {
            $collections_string = $collections_string.$collection->name.', ';
        }

        return view('overlays.are-you-sure', [
            'action' => '/admin/tag/delete?name='.request()->name,
            'page_view' => 'admin',
            'href' => '/admin',
            'title' => 'delete tag?',
            'text' => 'this tag will be removed from following collections: '.$collections_string
        ]);
    }

    public function delete() {
        $tag = Tag::query()->where('name', '=', request()->name)->first();

        $tag->collections()->detach();
        $tag->delete();

        return redirect('/admin');
    }
}
