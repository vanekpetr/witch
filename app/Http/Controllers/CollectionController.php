<?php


namespace App\Http\Controllers;


use App\Models\Collection;
use Illuminate\Support\Facades\File;

class CollectionController extends AdminController
{
    public function create() {
        return view('overlays.create-collection');
    }

    public function edit($url) {
        $collection = Collection::query()->where('url', '=', $url)->first();
        return view('collections.edit', [
            'collection' => $collection
        ]);
    }

    public function update($url) {
        $collection = Collection::query()->where('url', '=', $url)->first();

        Log:info($collection);

        $collection->name = request()->name;
        $collection->url = request()->url;
        $collection->description = request()->description;

        $collection->save();

        return redirect('/admin/collection/edit/'.$collection->url);
    }

    public function store() {
        //Log::alert('berofe validation');
        $data = request()->validate([
            'url' => 'required|unique:collections',
            'name' => 'required',
            'description' => '',
        ]);

        Collection::create($data);

        return redirect('/admin/collection/edit/'.$data['url']);
    }

    public function confirm_delete($url) {
        $collection = Collection::query()->where('url', '=', $url)->first();

        return view('overlays.are-you-sure', [
            'action' => '/admin/collection/delete?url='.$url,
            'page_view' => 'collections.edit',
            'collection' => $collection,
            'href' => '/admin/collection/edit/'.$url,
            'title' => 'delete collection?',
            'text' => $collection->name
        ]);
    }

    public function confirm_delete_out() {
        $collection = Collection::query()->where('url', '=', request()->url)->first();

        return view('overlays.are-you-sure', [
            'action' => '/admin/collection/delete?url='.request()->url,
            'page_view' => 'admin',
            'href' => '/admin',
            'title' => 'delete collection?',
            'text' => $collection->name
        ]);
    }

    public function delete() {
        $collection = Collection::query()->where('url', '=', request()->url)->first();

        $collection->tags()->detach();

        foreach($collection->photos()->get() as $photo) {
            File::delete(public_path().$photo->path);
        }

        $collection->photos()->delete();
        $collection->delete();

        return redirect('admin');
    }
}
