<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AnchorController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ArtisanController;
use App\Http\Controllers\Auth\TagController;
use App\Http\Controllers\BioController;
use App\Http\Controllers\CollectionController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\EditController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HeadingController;
use App\Http\Controllers\ImageResourceController;
use App\Http\Controllers\MasterUserController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TextResourceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ViewerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('web');
});*/

Auth::routes([
    'register' => false
]);

/* --- TŘI ČÁRKY --- */
Route::get('/admin/refresh_endpoint', [ArtisanController::class, 'refresh'])->name('cache_refresh');
/* --- ADMIN ROUTES --- */
Route::get('/admin', [AdminController::class, 'index'])->name('admin');
//Route::get('/admin/register', [AdminController::class, 'register'])->name('admin.register');
//Route::redirect('/register', '/admin/register');
Route::get('/admin/users', [AdminController::class, 'users'])->name('users.show');
//custom register routes
Route::get('/admin/users/create', [MasterUserController::class, 'create'])->name('user.create');
Route::post('/admin/users/create', [MasterUserController::class, 'store'])->name('user.store');
Route::get('/admin/users/edit', [UserController::class, 'edit'])->name('user.edit');
Route::post('/admin/users/edit', [UserController::class, 'update'])->name('user.update');
Route::get('/admin/users/delete', [MasterUserController::class, 'confirm_delete'])->name('user.confirm_delete');
Route::post('/admin/users/delete', [MasterUserController::class, 'delete'])->name('user.delete');
//collections administration
Route::get('/admin/collection/create', [CollectionController::class, 'create'])->name('collection.create');
Route::post('/admin/collection', [CollectionController::class, 'store'])->name('collection.store');
Route::get('/admin/collection/edit/{url}', [CollectionController::class, 'edit'])->name('collection.edit');
Route::post('/admin/collection/edit/{url}', [CollectionController::class, 'update'])
    ->name('collection.update');
Route::get('/admin/collection/edit/{url}/delete', [CollectionController::class, 'confirm_delete'])
    ->name('confirm_delete');
Route::get('/admin/collection/delete', [CollectionController::class, 'confirm_delete_out'])
    ->name('collection.confirm_delete_out');
Route::post('/admin/collection/delete', [CollectionController::class, 'delete'])->name('collection.delete');

Route::post('/admin/collection/edit/{collection_url}/photo', [PhotoController::class, 'store'])
    ->name('photo.store');
Route::post('/admin/collection/edit/{collection_url}/photo/delete', [PhotoController::class, 'delete'])
    ->name('photo.delete');
//tags administration
Route::post('/admin/collection/edit/{collection_url}/tag', [TagController::class, 'add'])->name('tag.add');
Route::post('/admin/collection/edit/{collection_url}/tag/remove', [TagController::class, 'remove'])
    ->name('tag.remove');
Route::post('/admin/tag/remove', [TagController::class, 'remove_collection'])->name('tag.remove_collection');
Route::get('/admin/tag/edit', [TagController::class, 'edit'])->name('tag.edit');
Route::post('/admin/tag/edit', [TagController::class, 'update'])->name('tag.update');
Route::get('/admin/tag/create', [TagController::class, 'create'])->name('tag.create');
Route::post('/admin/tag/create', [TagController::class, 'store'])->name('tag.store');
Route::get('/admin/tag/delete', [TagController::class, 'confirm_delete'])->name('tag.confirm_delete');
Route::post('/admin/tag/delete', [TagController::class, 'delete'])->name('tag.delete');
//implicit admin redirect route
Route::redirect('/admin/{any}','/admin');

/* --- VIEWER ROUTES --- */
Route::get('/photo/{photo_id}', [ViewerController::class, 'photo'])->name('photo.show');

Route::get('/view/g/{name}', [ViewerController::class, 'group'])->name('image_resource_group.show');
//Route::get('/view/!{name}', [ViewerController::class, 'tag'])->name('tag.show');
Route::get('/view/{url}', [ViewerController::class, 'collection'])->name('collection.show');

/* --- EDIT ROUTES --- */
Route::get('/edit/article', [ArticleController::class, 'edit'])->name('article.edit'); //TODO: change routes (create, store -> get:create, post:create ...)
Route::post('/edit/article/create', [ArticleController::class, 'create'])->name('article.create'); //TODO: should be GET ???
Route::post('/edit/article/store', [ArticleController::class, 'store'])->name('article.store');
Route::post('/edit/article/update', [ArticleController::class, 'update'])->name('article.update');
Route::get('/edit/article/delete', [ArticleController::class, 'confirm_delete'])->name('article.confirm_delete');
Route::post('/edit/article/delete', [ArticleController::class, 'delete'])->name('article.delete');
Route::post('/edit/article/move', [ArticleController::class, 'move'])->name('article.move');

Route::post('/edit/anchor/create', [AnchorController::class, 'create'])->name('anchor.create'); //TODO: should be GET ???
Route::post('/edit/anchor/store', [AnchorController::class, 'store'])->name('anchor.store');
Route::get('/edit/anchor', [AnchorController::class, 'edit'])->name('anchor.edit');
Route::post('/edit/anchor', [AnchorController::class, 'store'])->name('anchor.update');
Route::get('/edit/anchor/delete', [AnchorController::class, 'confirm_delete'])->name('anchor.confirm_delete');
Route::post('/edit/anchor/delete', [AnchorController::class, 'delete'])->name('anchor.delete');
Route::post('/edit/anchor/move', [AnchorController::class, 'move'])->name('anchor.move');

Route::post('/edit/gallery/create', [GalleryController::class, 'create'])->name('gallery.create');
Route::get('/edit/gallery', [GalleryController::class, 'edit'])->name('gallery.edit');
Route::get('/edit/gallery/delete', [GalleryController::class, 'confirm_delete'])->name('gallery.confirm_delete');
Route::post('/edit/gallery/delete', [GalleryController::class, 'delete'])->name('gallery.delete');
Route::post('/edit/gallery/move', [GalleryController::class, 'move'])->name('gallery.move');

Route::post('/edit/heading/create', [HeadingController::class, 'create'])->name('heading.create');
Route::post('/edit/heading/store', [HeadingController::class, 'store'])->name('heading.store');
Route::get('/edit/heading', [HeadingController::class, 'edit'])->name('heading.edit');
Route::post('/edit/heading', [HeadingController::class, 'update'])->name('heading.update');
Route::get('/edit/heading/delete', [HeadingController::class, 'confirm_delete'])->name('heading.confirm_delete');
Route::post('/edit/heading/delete', [HeadingController::class, 'delete'])->name('heading.delete');
Route::post('/edit/heading/move', [HeadingController::class, 'move'])->name('heading.move');

Route::get('/edit/contact', [ContactController::class, 'edit'])->name('contact.edit');
Route::post('/edit/contact', [ContactController::class, 'update'])->name('contact.update');
Route::get('/edit/contact/delete', [ContactController::class, 'confirm_delete'])->name('contact.confirm_delete');
Route::post('/edit/contact/delete', [ContactController::class, 'delete'])->name('contact.delete');
Route::post('/edit/contact/create', [ContactController::class, 'store'])->name('contact.store');

Route::get('/edit/bio/text', [BioController::class, 'edit_text'])->name('bio.edit_text');
Route::get('/edit/bio/image', [BioController::class, 'edit_image'])->name('bio.edit_image');

Route::get('/edit/service/create', [ServiceController::class, 'create'])->name('service.create');
Route::post('/edit/service/create', [ServiceController::class, 'store'])->name('service.store');
Route::get('/edit/service', [ServiceController::class, 'edit'])->name('service.edit');
Route::post('/edit/service', [ServiceController::class, 'update'])->name('service.update');
Route::get('/edit/service/delete', [ServiceController::class, 'confirm_delete'])->name('service.confirm_delete');
Route::post('/edit/service/delete', [ServiceController::class, 'delete'])->name('service.delete');

Route::get('/edit/service/image', [ServiceController::class, 'edit_title_image'])->name('service.edit_title_image');
Route::get('/edit/service/add_image', [ServiceController::class, 'add_title_image'])->name('service.add_title_image');
Route::get('/edit/service/remove_image', [ServiceController::class, 'remove_title_image'])->name('service.remove_title_image');
Route::get('/edit/service/sample_images', [ServiceController::class, 'edit_sample_images'])->name('service.edit_sample_images');

Route::post('/edit/service/move', [ServiceController::class, 'move'])->name('service.move');

/* --- RESOURCE ROUTES --- */
Route::get('/edit/text', [TextResourceController::class, 'edit'])->name('text.edit');
Route::post('/edit/text', [TextResourceController::class, 'update'])->name('text.update');
Route::get('/edit/image', [ImageResourceController::class, 'edit'])->name('image.edit');
Route::get('/edit/image/g', [ImageResourceController::class, 'edit_group'])->name('image-group.edit');
Route::post('/edit/image/g/create', [ImageResourceController::class, 'create'])->name('image.create');
Route::get('/edit/image/g/delete', [ImageResourceController::class, 'confirm_delete'])->name('image.confirm_delete');
Route::post('/edit/image/g/delete', [ImageResourceController::class, 'delete'])->name('image.delete');
Route::post('/edit/image', [ImageResourceController::class, 'update'])->name('image.update');
Route::post('/edit/image/link', [ImageResourceController::class, 'link'])->name('image.link');

/* --- PAGE EDIT ROUTES --- */
Route::get('/{page_name?}/edit', [EditController::class, 'index'])->name('page.edit');
Route::get('/edit', [EditController::class, 'index'])->name('web.edit');

/* --- SPECIAL ROUTES --- */
Route::get('/!{tag_name}', [PagesController::class, 'tag'])->name('tag.show');
Route::redirect('/contact', '/bio#contact');

/* --- WEB ROUTES --- */
Route::get('/{page_name?}', [PagesController::class, 'index'])->name('web');
