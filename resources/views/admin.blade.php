@extends('layouts.app')

@section('content')
@guest <!-- might not need this -->
    <button>
        <a class="nav-link" href="{{ route('login') }}">Login</a>
    </button>
@else
    <header class="admin-header">
        <img class="d-block mx-auto" style="height: 5rem;" src="/assets/witch__logo--2.svg" alt="W!TCH">
        <div style="font-family: 'W!TCH', 'Passion One', sans-serif; font-size: 2.1rem;" class="text-center text-uppercase">administrator</div>
    </header>
    <section class="admin-nav">
        <a role="button" href="/edit">edit web</a>
        <a role="button" href="/admin/users">manage users</a>
        <div class="filler"></div>
        <a role="button" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </section>
    <main class="admin-page page">
        @if (session('status'))
            <div style="padding: 2rem 0;" class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
            <div style="width: max-content; margin: 2rem auto;">logged in as: {{ Auth::user()->username }}</div>
        @include('admin-elements.tag-management')
        @include('admin-elements.collection-list')
    </main>
@endguest
@endsection
