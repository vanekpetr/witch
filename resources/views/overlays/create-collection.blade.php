@extends('overlays.overlay', [
    'page_view' => 'admin',
    'href' => '/admin'
])

@section('overlay-content')
<form class="create-form" action="/admin/collection" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="create-form__row">
        <label for="name">name:</label>
        <input name="name" type="text" />
    </div>
    @if ($errors->has('name'))
        <strong>required field</strong>
    @endif
    <br>
    <div class="create-form__row">
        <label for="url">custom url:</label>
        <div class="create-form__row__container">
            <span>witch.art/collection/</span>
            <input name="url" type="text" />
        </div>
    </div>
    @if ($errors->has('url'))
        <strong>required and unique field</strong>
    @endif
    <br>
    <div class="create-form__row">
        <label for="description">description:</label>
        <textarea name="description"></textarea>
    </div>
    <br>
    <input type="submit" value="save">
</form>
@endsection
