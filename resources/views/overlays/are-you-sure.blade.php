<!-- --- REQUIRED VARIABLES: $page_view, $href --- -->
<!-- --- OPTIONAL VARIABLES: $title, $text --- -->

@extends('overlays.overlay')

@section('overlay-content')
<form class="are-you-sure" action="{{$action}}" method="POST">
    @csrf
    @isset($title)
        <h3>{{$title}}</h3>
    @endisset
    @isset($text)
        <p>{{$text}}</p>
    @endisset
    <br>
    <div class="are-you-sure__buttons">
        <input type="submit" value="OK">
        <a role="button" href="{{$href}}">cancel</a>
    </div>
</form>
@endsection
