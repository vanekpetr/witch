@extends('overlays.users', [
    'page_view' => 'overlays.users',
    'href' => '/admin/users'
])

@section('overlay-content')
    <form class="create-form" action="/admin/users/edit?username={{request()->username}}" method="POST">
        @csrf
        <div class="create-form__row">
            <label>username: {{request()->username}}</label>
        </div>
        <br>
        <div class="create-form__row">
            <label for="password">new password: </label>
            <input name="password" type="password">
        </div>
        <br>
        <div class="create-form__row">
            <label for="password_confirmation">confirm new password: </label>
            <input name="password_confirmation" type="password">
        </div>
        <br>
        @error('password')
        <strong>error: {{$message}}</strong>
        <br><br>
        @enderror
        <button type="submit">save</button>
    </form>
@endsection
