@extends('overlays.overlay', [
    'page_view' => 'pages.'.$page->view,
    'href' => $page->route.'#'.$anchor
])

@section('overlay-content')
    @yield('select-resource')
    @isset($resource)
    <section class="edit-image">
        @isset($title)
            <h3>{{$title}}</h3>
        @endisset
        @isset($text)
            <p>{{$text}}</p>
        @endisset
        <form class="file-input" action="/edit/image?name={{$resource->name}}&page={{$page->name}}&anchor={{$anchor ?? ''}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file">
            <div class="space"></div>
            <div class="filler"></div>
            <input type="submit" value="upload">
        </form>
        <form class="link-photo" action="/edit/image/link?name={{$resource->name}}&page={{$page->name}}&anchor={{$anchor ?? ''}}" method="POST">
            @csrf
            <div class="link-photo__header">
                <input name="create_copy" type="checkbox">
                <label for="create_copy">create a copy</label>
                <div class="space"></div>
                <div class="filler"></div>
                <input type="submit" value="select">
            </div>
            @foreach($collections as $collection)
                <header>{{$collection->name}}</header>
                <div class="link-photo__collection__photos">
                @foreach($collection->photos()->get() as $photo)
                    <div class="link-photo__collection__photo">
                        <label for="photo_id__{{$photo->id}}">
                            <img src="{{$photo->path}}" alt="err">
                        </label>
                        <input type="radio" name="photo_id" id="photo_id__{{$photo->id}}" value="{{$photo->id}}">
                    </div>
                @endforeach
                </div>
            @endforeach
        </form>
    </section>
    @else
        NO RESOURCE SELECTED
    @endisset
@endsection
