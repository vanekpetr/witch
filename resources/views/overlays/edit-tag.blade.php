@extends('overlays.overlay', [
    'page_view' => 'admin',
    'href' => '/admin'
])

@section('overlay-content')
    <form action="/admin/tag/edit?original_name={{$tag->name}}" method="POST">
        @csrf
        <div class="inline-form">
            <input name="name" type="text" value="{{$tag->name}}" placeholder="tag name">
            <div class="grow"></div>
            <input type="submit" value="save">
        </div>
        <br><br>
        @foreach($tag->collections()->get() as $collection)
            <div style="padding: 0.33rem 1rem; background-color: #eeeeee;">
                <b>{{$collection->name}}</b>&nbsp;&nbsp;
                <span>(witch.art/collection/{{$collection->url}})</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <form style="display: inline !important;" action="/admin/tag/remove?collection_url={{$collection->url}}&name={{$tag->name}}" method="POST">
                    @csrf
                    <input style="color: red; border: 0; border-radius: 0; padding: 0; background: none;" type="submit" value="✕">
                </form>
            </div>
        @endforeach
    </form>
@endsection
