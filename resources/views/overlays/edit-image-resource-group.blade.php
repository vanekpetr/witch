@extends('overlays.edit-image-resource')

@section('select-resource')
    <section class="select-resource">
        @if($resource != null)
            @if($resource_group->where('name', '=', $resource->group)->first() != null)
                <div class="select-resource__item @if($resource_group->where('name', '=', $resource->group)->first()->name == $resource->name)select-resource__item--selected @endif">
                    <a href="/edit/image/g?name={{$resource->group}}&page={{$page->name}}&resource={{$resource_group->where('name', '=', $resource->group)->first()->name}}#{{$anchor}}">
                        <img src="{{$resource_group->where('name', '=', $resource->group)->first()->path}}" alt="err">
                        <span>title image</span>
                    </a>
                    <a class="delete-button" role="button" href="/edit/image/g/delete?name={{$resource_group->where('name', '=', $resource->group)->first()->name}}&page={{$page->name}}&anchor={{$anchor}}">✕</a>
                </div>
            @endif
            @foreach($resource_group->where('name', '!=', $resource->group) as $item)
                <div class="select-resource__item @if($item->name == $resource->name)select-resource__item--selected @endif">
                    <a href="/edit/image/g?name={{$resource->group}}&page={{$page->name}}&resource={{$item->name}}#{{$anchor}}">
                        <img src="{{$item->path}}" alt="err">
                        <span>{{$item->name}}</span>
                    </a>
                    <a class="delete-button" role="button" href="/edit/image/g/delete?name={{$item->name}}&page={{$page->name}}&anchor={{$anchor}}">✕</a>
                </div>
            @endforeach
        @endif
        <form
            class="select-resource__item select-resource__item--add"
            action="/edit/image/g/create?group={{request()->name ?? $resource_group->first()->group}}&page={{$page->name}}&anchor={{$anchor}}"
            method="POST"
        >
            @csrf
            <input type="submit" value="+ add image">
        </form>
    </section>
    <br>
    <br>
@endsection
