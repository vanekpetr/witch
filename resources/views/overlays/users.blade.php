@extends('overlays.overlay', [
    'page_view' => 'admin',
    'href' => '/admin'
])

@section('overlay-content')
<header class="users-header" style="width: 90vw; max-width: 500px">
    <b>MANAGE USERS</b>
    <div class="space"></div>
    <div class="filler"></div>
    @if(Auth::id() == \App\Models\User::query()->where('username', '=', $master_username)->first()->id)
        <a role="button" href="/admin/users/create">add a user</a>
    @endif
</header>
<section class="users" style="width: 90vw; max-width: 500px">
    <div class="user">
        <span>{{Auth::user()->username}} (you)</span>
        <div class="space"></div>
        <div class="filler"></div>
        <a role="button" href="/admin/users/edit?username={{Auth::user()->username}}">change password</a>
    </div>
    @foreach ($users as $user)
        @if(Auth::id() != $user->id)
            <div class="user">
                <span>{{$user->username}}</span>
                <div class="space"></div>
                <div class="space"></div>
                <div class="space"></div>
                <div class="space"></div>
                <div class="space"></div>
                <div class="space"></div>
                <div class="filler"></div>
                @if(Auth::id() == \App\Models\User::query()->where('username', '=', $master_username)->first()->id)
                    <a role="button" href="/admin/users/edit?username={{$user->username}}">change password</a>
                @else
                    <a role="button" style="visibility: hidden" href="">change password</a>
                @endif
                @if(Auth::id() == \App\Models\User::query()->where('username', '=', $master_username)->first()->id)
                    <a class="delete-button" role="button" href="/admin/users/delete?username={{$user->username}}">✕</a>
                @else
                    <a role="button" style="visibility: hidden;">✕</a>
                @endif
            </div>
        @endif
    @endforeach
</section>
@endsection
