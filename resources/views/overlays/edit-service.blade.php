@extends('overlays.overlay', [
    'page_view' => 'pages.services',
    'href' => '/sluzby#service'.$service->position
])

@section('overlay-content')
    <form class="edit-form" action="/edit/service?id={{$service->id}}" method="POST">
        @csrf
        <input name="title" type="text" placeholder="service title" value="{{$service->title}}">
        <br>
        <input name="pricing" type="text" placeholder="service pricing" value="{{$service->pricing}}">
        <br>
        <textarea name="description" placeholder="service description">{{$service->description}}</textarea>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
