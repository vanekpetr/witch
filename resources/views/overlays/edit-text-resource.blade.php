@extends('overlays.overlay', [
    'page_view' => $view,
    'href' => $route.'#'.$anchor
])

@section('overlay-content')
    <form class="edit-form" action="/edit/text?name={{$resource->name}}&href={{$route.'#'.$anchor}}" method="POST">
        @csrf
        @isset($title)
            <h3>{{$title}}</h3>
        @endisset
        @isset($text)
            <p>{{$text}}</p>
        @endisset
        <textarea name="resource_content" id="resource_content">{{$resource->content}}</textarea>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
