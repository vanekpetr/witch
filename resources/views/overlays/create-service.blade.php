@extends('overlays.overlay', [
    'page_view' => 'pages.services',
    'href' => '/sluzby#add-service'.request()->position
])

@section('overlay-content')
    <form class="create-form" action="/edit/service/create?position={{request()->position}}" method="POST">
        @csrf
        <div class="create-form__row">
            <label for="title">service title</label>
            <input name="title" type="text">
        </div>
        <br>
        <div class="create-form__row">
            <label for="pricing">service pricing</label>
            <input name="pricing" type="text">
        </div>
        <br>
        <div class="create-form__row">
            <label for="description">service description</label>
            <textarea name="description"></textarea>
        </div>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
