<!-- --- REQUIRED VARIABLES: $article, $page --- -->

@extends('overlays.overlay')

{{
    $page_view = "pages.".$page->view,
    $href = $page->route."#article".$article->id
}}

@section('overlay-content')
    <form class="edit-form" action="/edit/article/update?id={{$article->id}}" method="POST">
        @csrf
        <input name="title" type="text" value="{{$article->title}}" placeholder="article title"><br>
        <input name="subtitle" type="text" value="{{$article->subtitle}}" placeholder="article subtitle"><br>
        <textarea name="text" placeholder="article text">{{$article->text}}</textarea><br>
        <input type="submit" value="save">
    </form>
@endsection
