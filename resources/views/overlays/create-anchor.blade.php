<!-- --- REQUIRED VARIABLES: $page --- -->

@extends('overlays.overlay')

{{
    $page_view = "pages.".$page->view,
    $href = $page->route."#add-element".request()->position
}}

@section('overlay-content')
    <form class="create-form" action="/edit/anchor/store?page_name={{$page->name}}&position={{request()->position}}" method="POST">
        @csrf
        <div class="create-form__row">
            <label for="name">anchor name:</label>
            <input name="name" type="text">
        </div>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
