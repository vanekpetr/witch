<!-- --- REQUIRED VARIABLES: $page --- -->

@extends('overlays.overlay')

{{
    $page_view = "pages.".$page->view,
    $href = $page->route."#add-element".request()->position
}}

@section('overlay-content')
    <form class="create-form" action="/edit/article/store?page_name={{$page->name}}&position={{request()->position}}" method="POST">
        @csrf
        <div class="create-form__row">
            <label for="title">article title:</label>
            <input name="title" type="text">
        </div>
        <br>
        <div class="create-form__row">
            <label for="subtitle">article subtitle:</label>
            <input name="subtitle" type="text">
        </div>
        <br>
        <div class="create-form__row">
            <label for="text">article text:</label>
            <textarea name="text"></textarea>
        </div>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
