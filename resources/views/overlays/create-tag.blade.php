@extends('overlays.overlay', [
    'page_view' => 'admin',
    'href' => '/admin'
])

@section('overlay-content')
    <form class="create-form" action="/admin/tag/create" method="POST">
        @csrf
        <div class="create-form__row">
            <label for="name">tag name:</label>
            <span class="create-form__row__container">
                <span>!</span>
                <input name="name" type="text">
            </span>
        </div>
        <br>
        <input type="submit" value="save">
    </form>
@endsection
