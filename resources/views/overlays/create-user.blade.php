@extends('overlays.overlay', [
    'page_view' => 'admin',
    'href' => '/admin'
])

@section('overlay-content')
    <form class="create-form" action="/admin/users/create" method="POST">
        @csrf
        <div class="create-form__row">
            <label for="username">username:</label>
            <input type="text" name="username" class="@error('username') is-invalid @enderror" required autocomplete="username" autofocus>
        </div>
        @error('username')
            <br>
            <strong>{{ $message }}</strong>
            <br>
        @enderror
        <br>
        <div class="create-form__row">
            <label for="email">email:</label>
            <input type="email" name="email" class="@error('email') is-invalid @enderror" required autocomplete="email">
        </div>
        @error('email')
            <br>
            <strong>{{ $message }}</strong>
            <br>
        @enderror
        <br>
        <div class="create-form__row">
            <label for="password">password</label>
            <input type="password" name="password" class="@error('email') is-invalid @enderror" required autocomplete="new-password">
        </div>
        @error('password')
            <br>
            <strong>{{ $message }}</strong>
            <br>
        @enderror
        <br>
        <div class="create-form__row">
            <label for="password_confirmation">confirm password:</label>
            <input type="password" name="password_confirmation" required autocomplete="new-password">
        </div>
        <br>
        <button type="submit">register</button>
    </form>
@endsection
