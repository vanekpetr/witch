@extends('overlays.overlay', [
    'page_view' => 'pages.bio',
    'href' => '/bio#contact'
])

@section('overlay-content')
        <form class="create-contact" action="/edit/contact/create" method="POST">
            @csrf
            <span class="create-contact__column">
                <label for="type">type:</label><br>
                <select name="type" id="type">
                    <option value="text">plain text</option>
                    <option value="https://">url</option>
                    <option value="mailto:">mail</option>
                    <option value="tel:">phone</option>
                    <option value="">any</option>
                </select>
            </span>
            <span class="create-contact__column">
                <label for="text">text:</label><br>
                <input name="text" type="text">
            </span>
            <span class="create-contact__column">
                <label for="link">link:</label><br>
                <input type="text" name="link">
            </span>
            <span class="create-contact__column">
                <label for="link_text">custom link text:</label><br>
                <input type="text" name="link_text">
            </span>
            <span class="create-contact__column">
                <input type="submit" value="+ add">
            </span>
        </form>
        <br>
    @foreach($contacts as $contact)
        <form class="edit-contact" action="/edit/contact?id={{$contact->id}}" method="POST">
            @csrf
            <select name="type" id="type">
                <option value="text" @if($contact->type == 'text') selected @endif>plain text</option>
                <option value="https://" @if($contact->type == 'https://') selected @endif>url</option>
                <option value="mailto:" @if($contact->type == 'mailto:') selected @endif>mail</option>
                <option value="tel:" @if($contact->type == 'tel:') selected @endif>phone</option>
                <option value="" @if($contact->type == '') selected @endif>any</option>
            </select>
            <input name="text" type="text" value="{{$contact->text}}" placeholder="text">
            <input type="text" name="link" value="{{$contact->link}}" placeholder="link">
            <input type="text" name="link_text" value="{{$contact->link_text}}" placeholder="custom link text">
            <input type="submit" value="save">
            <a role="button" class="delete-button" href="/edit/contact/delete?id={{$contact->id}}">delete</a>
        </form>
    @endforeach
@endsection

