@extends('overlays.overlay', [
    'page_view' => 'pages.'.$page->view,
    'href' => $page->route.'#heading'.$heading->id
])

@section('overlay-content')
    <form class="edit-form" action="/edit/heading?id={{$heading->id}}" method="POST">
        @csrf
        <input name="title" type="text" placeholder="heading title" value="{{$heading->title}}">
        <input name="subtitle" type="text" placeholder="heading subtitle" value="{{$heading->subtitle}}">
        <input type="submit" value="save">
    </form>
@endsection
