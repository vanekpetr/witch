<!-- --- REQUIRED VARIABLES: $page_view, $href --- -->

@extends($page_view, [ 'displaying_overlay' => true ])

<aside class="overlay">
    <a href="{{$href}}" class="overlay__background"></a>
    <section class="overlay__content {{$content_class ?? 'overlay-content-class--normal'}}">
        @yield('overlay-content')
    </section>
</aside>

<script>
    document.onkeydown = (e) => {
        if(e.key === 'Escape') {
            window.location.href = '{{$href}}';
        }
    }
</script>
