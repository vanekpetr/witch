<br><br>
<a class="anchor" id="heading{{$heading->id}}"></a>
@auth
    <header class="heading__header--edit edit-header">
        <span>heading edit header</span>
        <div class="space"></div>
        <a role="button" href="/edit/heading?id={{$heading->id}}#heading{{$heading->id}}">edit this heading</a>
        <a class="delete-button" role="button" href="/edit/heading/delete?id={{$heading->id}}#heading{{$heading->id}}">delete this heading</a>
        <div class="filler"></div>
        @if($moving)
            <a class="delete-button" role="button" href="{{$heading->page()->first()->route}}#heading{{$heading->id}}">cancel moving</a>
        @else
            <a role="button" href="{{$heading->page()->first()->route}}?move_element_from_position={{$heading->position}}&page_name={{$heading->page_name}}&type=heading#heading{{$heading->id}}">move</a>
        @endif
    </header>
@endauth
<hgroup class="heading">
    <h2>{{$heading->title}}</h2>
    @isset($heading->subtitle)
        <p>{{$heading->subtitle}}</p>
    @endisset
</hgroup>
