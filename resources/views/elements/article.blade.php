<!-- --- REQUIRED VARIABLES: $article --- -->
<a class="anchor" id="article{{$article->id}}"></a>
<article>
    @auth
        <header class="article__header--edit edit-header">
            <span>article edit header</span>
            <div class="space"></div>
            <a role="button" href="/edit/article?id={{$article->id}}#article{{$article->id}}">edit this article</a>
            <a role="button" class="delete-button" href="/edit/article/delete?id={{$article->id}}#article{{$article->id}}">delete this article</a>
            <div class="filler"></div>
            @if($moving)
                <a class="delete-button" role="button" href="{{$article->page()->first()->route}}#article{{$article->id}}">cancel moving</a>
            @else
                <a role="button" href="{{$article->page()->first()->route}}?move_element_from_position={{$article->position}}&page_name={{$article->page_name}}&type=article#article{{$article->id}}">move</a>
            @endif
        </header>
    @endauth
    <hgroup class="article__header">
        @isset($article->title)
            <h2 class="article__title">
                {{$article->title}}
            </h2>
        @endisset
        @isset($article->subtitle)
            <h5 class="article__subtitle">
                {{$article->subtitle}}
            </h5>
        @endisset
    </hgroup>
    @isset($article->text)
        <p class="article__text">
            {{$article->text}}
        </p>
    @endisset
    <br><br><br>
</article>
