@auth
    <header class="contact__header--edit edit-header">
        <span>contact edit header</span>&nbsp;&nbsp;&nbsp;&nbsp;
        <a role="button" href="/edit/contact">edit contact</a>
    </header>
@endauth
<section class="contact" id="contact">
    <h2>KONTAKT</h2>
    <p>
        <span>vojta janovič</span>
        <br>
        @foreach($contacts as $contact)
            <span style="white-space: nowrap">
                <span>{{$contact->text}}: </span>
                @if($contact->type == 'text')
                    <span>{{$contact->link_text}}</span>
                @else
                    <a href="{{$contact->type}}{{$contact->link}}">{{$contact->link_text}}</a>
                @endif
            </span>
            <br>
        @endforeach
    </p>
</section>
