@auth
    <header class="article__header--edit edit-header">
        <span>service edit header</span>
        <div class="space"></div>
        <a role="button" href="/edit/service?id={{$service->id}}#service{{$service->id}}">edit text</a>
        <a role="button" href="/edit/service/sample_images?id={{$service->id}}&page=service#service{{$service->id}}">edit images</a>
        @if($image_resource_class::get_image('service'.$service->id) != null)
            <a class="delete-button" role="button" href="/edit/service/remove_image?id={{$service->id}}">remove title image</a>
        @else
            <a role="button" href="/edit/service/add_image?id={{$service->id}}#service{{$service->id}}">add title image</a>
        @endif
        <a class="delete-button" role="button" href="/edit/service/delete?id={{$service->id}}#service{{$service->id}}">delete this service</a>
        <div class="filler"></div>
        @if($moving)
            <a role="button" href="/sluzby">cancel moving</a>
        @else
            <a role="button" href="/sluzby?move_service_from_position={{$service->position}}#service{{$service->id}}">move</a>
        @endif
    </header>
@endauth
<a class="anchor" id="service{{$service->id}}"></a>
<section class="service">
    @if($image_resource_class::get_image('service'.$service->id) != null)
        <img class="service__title-image" src="{{$image_resource_class::get_image('service'.$service->id)}}" alt="err">
    @endif
    <div class="service__content">
        <hgroup class="service__header">
            <h2 class="service__title">
                {{$service->title}}
            </h2>
            @isset($service->pricing)
                <h5 class="service__subtitle">
                    {{$service->pricing}}
                </h5>
            @endisset
        </hgroup>
        @isset($service->description)
            <br>
            <p class="service__description">
                {{$service->description}}
            </p>
        @endisset
        @if($image_resource_class::get_group('service'.$service->id) != null)
            <br><br>
            @foreach($image_resource_class::get_group('service'.$service->id)->where('name', '!=', 'service'.$service->id) as $image)
                <a href="/view/g/service{{$service->id}}?id={{$image->id}}&title={{$service->title}}&from=/sluzby">
                    <img src="{{$image->path}}" alt="err">
                </a>
            @endforeach
        @endif
    </div>
</section>
<br><br><br>
