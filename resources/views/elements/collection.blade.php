<!-- --- REQUIRED VARIABLES: $collection --- -->

<section class="collection" id="{{$collection->url}}">
    @auth
        <header class="collection__header--edit edit-header">
            <span>collection edit header</span>&nbsp;&nbsp;&nbsp;&nbsp;
            <a role="button" href="/admin/collection/edit/{{$collection->url}}">edit this collection</a>
        </header>
    @endauth
    <header class="collection__header">
        <hgroup>
            <h2 class="collection__name">
                <a href="/view/{{$collection->url}}">{{$collection->name}}</a>
            </h2>
            <h5 class="collection__tags">
                &nbsp;
                @foreach($collection->tags()->get() as $tag)
                    <a href="/!{{$tag->name}}" class="stripe-link--tag">{{$tag->name}}</a>&nbsp;
                @endforeach
            </h5>
        </hgroup>
        <p class="collection__description">{{$collection->description}}</p>
    </header>
    <div class="photos">
        @include('builders.photos', [ 'collection' => $collection ])
    </div>
</section>
