<!-- --- REQUIRED VARIABLES: $anchor --- -->

<a class="anchor" id="{{$anchor->name}}"></a>

@auth
    <header class="anchor__header--edit edit-header">
        <span>anchor edit header: #{{$anchor->name}}</span>
        <div class="space"></div>
{{--        <a role="button" href="/edit/anchor?name={{$anchor->name}}#{{$anchor->name}}">edit this anchor</a>--}}
        <a role="button" class="delete-button" href="/edit/anchor/delete?name={{$anchor->name}}#{{$anchor->name}}">delete this anchor</a>
        <div class="filler"></div>
        @if($moving)
            <a class="delete-button" role="button" href="{{$anchor->page()->first()->route}}#{{$anchor->name}}">cancel moving</a>
        @else
            <a role="button" href="{{$anchor->page()->first()->route}}?move_element_from_position={{$anchor->position}}&page_name={{$anchor->page_name}}&type=anchor#{{$anchor->name}}">move</a>
        @endif
    </header>
    <br><br><br>
@endauth
