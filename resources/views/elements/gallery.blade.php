<a class="anchor" id="gallery{{$gallery->id}}"></a>
@auth
    <header class="gallery__header--edit edit-header">
        <span>gallery edit header</span>
        <div class="space"></div>
        <a role="button" href="/edit/image/g?name=gallery{{$gallery->id}}&page=home&anchor=gallery{{$gallery->id}}#gallery{{$gallery->id}}">edit this gallery</a>
        <a role="button" class="delete-button" href="/edit/gallery/delete?id={{$gallery->id}}#gallery{{$gallery->id}}">delete this gallery</a>
        <div class="filler"></div>
        @if($moving)
            <a class="delete-button" role="button" href="{{$gallery->page()->first()->route}}#gallery{{$gallery->id}}">cancel moving</a>
        @else
            <a role="button" href="{{$gallery->page()->first()->route}}?move_element_from_position={{$gallery->position}}&page_name={{$gallery->page_name}}&type=gallery#gallery{{$gallery->id}}">move</a>
        @endif
    </header>
@endauth
<section class="gallery">
    @foreach($image_resource_class::get_group('gallery'.$gallery->id) as $image)
        <a href="/view/g/gallery{{$gallery->id}}?id={{$image->id}}&title=galerie">
            <img src="{{$image->path}}" alt="err">
        </a>
    @endforeach
</section>
<br><br><br>
