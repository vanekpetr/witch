@extends('web')

@section('title', 'W!TCH -- login as admin')

@section('page')
    <br><br><br><br><br>
    <img class="d-block mx-auto" style="height: 5rem;" src="assets/witch__logo--2.svg" alt="W!TCH">
    <br>
    <div style="font-family: 'W!TCH', 'Passion One', sans-serif; font-size: 2.1rem;" class="text-center text-uppercase">administrator</div>
    <br><br>
    <form class="login-form" method="POST" action="{{ route('login') }}">
        @csrf
        <input placeholder="username"
               id="username"
               type="text"
               class="@error('username') is-invalid @enderror"
               name="username"
               value="{{ old('username') }}"
               required
               autocomplete="username"
               autofocus>
        @error('username')
            <span class="text-center invalid-feedback" role="alert">{{ $message }}</span>
        @enderror
        <br>
        <input placeholder="password"
               id="password"
               type="password"
               class="@error('password') is-invalid @enderror"
               name="password"
               required
               autocomplete="current-password">
        @error('password')
            <span class="text-center invalid-feedback" role="alert">{{ $message }}</span>
        @enderror
        <br>
        <div class="login-form__row">
            <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            &nbsp;
            <label class="" for="remember">remember me</label>
        </div>
        <br><br>
        <button type="submit">login</button>
    </form>
@endsection

@section('footer')
    <!---->
@endsection
