@extends('layouts.app')

@section('title')
    @yield('title', 'W!TCH photo and video production')
@endsection

@section('content')
    @hasSection('navbar')
        @yield('navbar')
    @else
        @include('elements.navbar')
    @endif

    @yield('page')

    @hasSection('footer')
        @yield('footer')
    @else
        @include('elements.footer')
    @endif

    @include('admin-elements.admin-bar')
@endsection
