<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.png">
    <link rel="icon" href="/favicon.png">
    <title>admin refresh endpoint</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Passion+One&display=swap');
        @font-face {
            font-family: 'W!TCH';
            src: url("public/font/witch.ttf");
        }

        * {
            font-family: 'W!TCH', 'Passion One', serif;
        }
        body, html {
            height: 100%;
            padding: 0;
            margin: 0;
        }
        body {
            background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSW97-ifkFGDvPowru6a0WqBFSUWjdWBBBqaQ&usqp=CAU);
            background-size: 15%;

            display: flex;
            justify-content: center;
            align-items: center;

            overflow: hidden;
        }
        body div {
            font-size: 150px;
            overflow: hidden;
            animation-name: animation;
            animation-duration: 3s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            transform-origin: 50% 45%;
            /*animation-direction: alternate;*/
        }
        @keyframes animation {
            0% {
                transform: rotate3d(0, 0, 0, 0);
                color: fuchsia;
                text-shadow: 7px 7px 5px greenyellow;
            }
            25% {
                transform: rotate3d(1, 1, 1, 90deg);
            }
            50% {
                transform: rotate3d(1, 1, 1, 180deg);
                color: greenyellow;
                text-shadow: 7px 7px 5px fuchsia;
            }
            75% {
                transform: rotate3d(1, 1, 1, 270deg);
            }
            100% {
                transform: rotate3d(1, 1, 1, 360deg);
                color: fuchsia;
                text-shadow: 7px 7px 5px greenyellow;
            }
        }
    </style>
</head>
<body>
<div>
    REFRESHED!
</div>
</body>
</html>
