@extends('layouts.app')

@section('content')
<header class="edit-collection-header">
    <span>edit collection "<b>{{$collection->name}}</b>"</span>
    <div class="space"></div>
    <a role="button" href="/admin">back to admin</a>
    <div class="filler"></div>
    <a role="button" class="delete-button" href="/admin/collection/edit/{{$collection->url}}/delete">delete this collection</a>
</header>
<br>
<main class="edit-collection page">
    <form class="edit-collection__info" action="/admin/collection/edit/{{$collection->url}}" enctype="multipart/form-data" method="POST">
        @csrf
        <input name="name" type="text" placeholder="collection name" value="{{$collection->name}}"/>
        @if ($errors->has('name'))
            <br>
            <strong>required field</strong>
            <br>
        @endif
        <br>
        <div class="edit-collection__inline-form">
            <span>janovic.media/collection/</span>&nbsp;
            <input name="url" type="text" placeholder="collection custom url" value="{{$collection->url}}"/>&nbsp;&nbsp;&nbsp;
            <span>not recommended to change</span>
        </div>
        @if ($errors->has('url'))
            <br>
            <strong>required and unique field</strong>
            <br>
        @endif
        <br>
        <textarea name="description" placeholder="collection description">{{$collection->description}}</textarea>
        <br>
        <div class="edit-collection__inline-form">
            <input type="submit" value="save">
        </div>
    </form>
    <br>
    <br>
    <br>
    <form class="edit-collection__inline-form" action="/admin/collection/edit/{{$collection->url}}/tag" method="POST">
        @csrf
        <span style="font-weight: bold">collection tags</span>
        <div class="space"></div>
        <div class="space"></div>
        <div class="space"></div>
        <span>!</span>&nbsp;
        <input name="name" type="text" list="tag-list">
        <datalist id="tag-list">
        @foreach($tags as $tag) <!-- $tags is a global variable defined in AppServiceProvider.php -->
            @if($collection->tags()->where('name', '=', $tag->name)->first() == null)
                <option value="{{$tag->name}}">
            @endif
            @endforeach
        </datalist>
        <input type="submit" value="+ add a tag">
    </form>
    <br>
    <div class="edit-collection__tags">
        @foreach($collection->tags()->get() as $tag)
            <span class="tag">
                <span>!{{$tag->name}}</span>&nbsp;
                <form action="/admin/collection/edit/{{$collection->url}}/tag/remove?name={{$tag->name}}" method="POST">
                    @csrf
                    <input class="delete-button" type="submit" value="✕">
                </form>
            </span>
        @endforeach
    </div>
    <br>
    <hr>
    <br>
    <file-uploader-component collection_id="{{$collection->url}}"></file-uploader-component>
    <br>
    <section class="photos">
        @foreach($collection->photos()->get() as $photo)
            <span class="photo">
                <form action="/admin/collection/edit/{{$collection->url}}/photo/delete?id={{$photo->id}}" method="POST">
                    @csrf
                    <input type="submit" value="✕">
                </form>
                <a href=""><img src="{{$photo->path}}" alt="err"></a>
            </span>
        @endforeach
    </section>
</main>
@endsection
