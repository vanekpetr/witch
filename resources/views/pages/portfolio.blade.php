@extends('web')

@section('title', 'W!TCH -- portfolio')

@section('page')
    <main class="page page--portfolio">
        <header class="portfolio__header">
            <h2>
                @isset($show_tag)
                    !{{$show_tag->name}}
                @else
                    PORTFOLIO
                @endisset
            </h2>
            <p>
                @isset($show_tag)
                    <a href="/portfolio" class="stripe-link">/PORTFOLIO</a>
                @endisset
                @foreach($tags as $tag)
                    @isset($show_tag)
                        @if($tag != $show_tag)
                            <a href="/!{{$tag->name}}" class="stripe-link--tag">{{$tag->name}}</a>
                        @endif
                    @else
                        <a href="/!{{$tag->name}}" class="stripe-link--tag">{{$tag->name}}</a>
                    @endisset
                @endforeach
            </p>
        </header>
        @include('builders.collections', [ 'selected_collections' => $selected_collections ?? $collections ])
    </main>
@endsection

@section('footer')
    <!---->
@endsection
