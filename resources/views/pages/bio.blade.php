@extends('web')

@section('page')
    <main class="page page--bio">
        <header>
            @auth
                <header class="bio__header--edit edit-header">
                    <span>bio edit header</span>
                    <div class="space"></div>
                    <a role="button" href="/edit/bio/text">edit bio</a>
                    <a role="button" href="/edit/bio/image">edit bio image</a>
                </header>
            @endauth
            <section class="bio">
                <p>
                    {{$text_resource_class::get_text('bio')}}
                </p>
                <img src="{{$image_resource_class::get_image('bio')}}" alt="err">
            </section>
            @include('elements.contact')
        </header>
        @include('builders.page-builder', [ 'page_name' => 'bio' ])
    </main>
@endsection
