@extends('web')

@section('navbar')
    <!---->
@endsection

@section('page')
    <main class="page page--home">
        @auth
            <!--<header style="position: absolute; top: 0; left: 0; width: 100vw;" class="banner__header--edit edit-header">
                <span>banner edit header</span>
                <div class="space"></div>
                <a role="button" href="">edit background image</a>
                <a role="button" class="delete-button" href="">remove background image</a>
            </header>-->
        @endauth
        <header class="home__header">
            <div class="home__header__content">
                <h2>W!TCH</h2>
                <p>
                    photo and video production&nbsp;&nbsp;&nbsp;
                    @foreach($pages->where('name', '!=', 'home') as $page)
                        <a href="{{$page->route}}" class="stripe-link--page">{{$page->route}}</a>
                    @endforeach
                    @foreach($anchors as $anchor)
                        <a href="#{{$anchor->name}}" class="stripe-link--anchor">{{$anchor->name}}</a>
                    @endforeach
                    <a href="http://instagram.com/witch.pics" class="stripe-link--outside">instagram</a>
                    <a href="https://www.youtube.com/channel/UCMWmQjFKaLcKkKFIsMeEo7Q" class="stripe-link--outside">youtube</a>
                </p>
            </div>
        </header>
        <br>
        <br>
        @include('builders.page-builder', [ 'page_name' => 'home' ])
    </main>
@endsection
