@extends('web')

@section('page')
    <main class="page page--services">
        <br><br>
        @include('builders.services-builder')
        <br><br><br>
        @auth @if(request()->move_service_from_position != null || request()->move_element_from_position != null)
            <hr style="border-width: 2px; border-color: black;">
            <br><br><br><br><br>
        @endif @endauth
        @include('builders.page-builder', [ 'page_name' => 'services' ])
    </main>
@endsection
