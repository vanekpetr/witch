@extends('layouts.app')

@section('content')
<main class="viewer">
    <a class="viewer__close-button" href="{{$from}}" role="button">✕</a>
    <header class="viewer__header">
{{--        <div>fotka z kolekce:</div>--}}
        <h2>
            @if(isset($title_href))
                <a href="{{$title_href}}">{{$title}}</a>
            @else
                {{$title}}
            @endif
        </h2>
        <h5>
            @if(isset($subtitle_tags))
                @foreach($subtitle_tags as $tag)
                    <a href="/!{{$tag->name}}">!{{$tag->name}}</a>
                @endforeach
            @elseif(isset($subtitle_href) && isset($subtitle))
                <a href="{{$subtitle_href}}">{{$subtitle}}</a>
            @else
                {{$subtitle ?? ''}}
            @endif
            <span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                {{$counter ?? ''}}
            </span>
        </h5>
    </header>
    <section class="viewer__display">
        @if(isset($previous))
            <a href="{{$previous}}">{{$previous_text ?? 'předchozí'}}</a>
        @else
            <a style="visibility: hidden;">předchozí</a>
        @endif
        <img src="{{$path}}" alt="chyba při načítání fotky">
        @if(isset($next))
            <a href="{{$next}}">{{$next_text ?? 'další'}}</a>
        @else
            <a style="visibility: hidden;">další</a>
        @endif
    </section>
</main>
@endsection
