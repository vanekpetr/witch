<!-- --- NOT-REQUIRED VARIABLES: $collections --- -->

@foreach($selected_collections ?? $collections as $collection) <!-- $collections <= global variable, defined in AppServiceProvider.php -->
    @include('elements.collection', [ 'collection' => $collection ])
@endforeach
