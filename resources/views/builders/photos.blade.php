<!-- --- REQUIRED VARIABLES: $collection --- -->

@foreach($collection->photos()->get() as $photo)
    @include('elements.photo', [ 'photo' => $photo ])
@endforeach
