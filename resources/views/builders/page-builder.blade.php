<!-- --- REQUIRED VARIABLES: $page_name --- -->

@auth
    @isset(request()->move_element_from_position)
        @include('admin-elements.move-element-here', [
            'page_name' => $page_name,
            'position' => 1,
            'move_element_from_position' => request()->move_element_from_position,
            'type' => request()->type
        ])
    @else
        @include('admin-elements.add-element', [
            'page_name' => $page_name,
            'position' => 1
        ])
    @endisset
@endauth

@for($i = 1 ;; $i++)
    @if($articles->where('page_name', '=', $page_name)->where('position', '=', $i)->first() !== null)
        @include('elements.article', [
            'article' => $articles->where('page_name', '=', $page_name)->where('position', '=', $i)->first(),
            'moving' => request()->move_element_from_position == $i
        ])
    @elseif($anchors->where('page_name', '=', $page_name)->where('position', '=', $i)->first() !== null)
        @include('elements.anchor', [
            'anchor' => $anchors->where('page_name', '=', $page_name)->where('position', '=', $i)->first(),
            'moving' => request()->move_element_from_position == $i
        ])
    @elseif($galleries->where('page_name', '=', $page_name)->where('position', '=', $i)->first() !== null)
        @include('elements.gallery', [
            'gallery' => $galleries->where('page_name', '=', $page_name)->where('position', '=', $i)->first(),
            'moving' => request()->move_element_from_position == $i
        ])
    @elseif($headings->where('page_name', '=', $page_name)->where('position', '=', $i)->first() !== null)
        @include('elements.heading', [
            'heading' => $headings->where('page_name', '=', $page_name)->where('position', '=', $i)->first(),
            'moving' => request()->move_element_from_position == $i
        ])
    @else
        @break
    @endif

    @isset(request()->move_element_from_position)
        @include('admin-elements.move-element-here', [
            'page_name' => $page_name,
            'position' => $i + 1,
            'move_element_from_position' => request()->move_element_from_position,
            'type' => request()->type
        ])
    @else
        @include('admin-elements.add-element', [
            'page_name' => $page_name,
            'position' => $i + 1
        ])
    @endisset

@endfor

{{--@foreach($articles->where('page_name', '=', $page_name) as $article)--}}
{{--    @include('elements.article', [--}}
{{--        'article' => $article--}}
{{--    ])--}}
{{--    @auth--}}
{{--    <br>--}}
{{--    @include('admin-elements.add-element', [--}}
{{--        'page_name' => $page_name,--}}
{{--        'position' => $article->position + 1--}}
{{--    ])--}}
{{--    @endauth--}}
{{--    <br><br>--}}
{{--@endforeach--}}
