@auth
    @isset(request()->move_service_from_position)
        @include('admin-elements.move-service-here', [
            'move_service_from_position' => request()->move_service_from_position,
            'position' => 1
        ])
    @else
        @include('admin-elements.add-service', ['position' => 1])
    @endisset
@endauth
@foreach($services as $service)
    @include('elements.service', [
        'service' => $service,
        'moving' => request()->move_service_from_position == $service->position
    ])
    @auth
    @isset(request()->move_service_from_position)
        @include('admin-elements.move-service-here', [
            'move_service_from_position' => request()->move_service_from_position,
            'position' => $service->position + 1
        ])
    @else
        @include('admin-elements.add-service', ['position' => $service->position + 1])
    @endisset
    @endauth
@endforeach
