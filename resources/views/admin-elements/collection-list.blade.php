<div class="collection-list">
    <header class="collection-list__header">
        <b>COLLECTION MANAGEMENT</b>
        <div class="space"></div>
        <a role="button" href="{{route('collection.create')}}">add a collection</a>
    </header>
    <br>
    <br>
    @foreach ($collections as $collection)
        <div class="collection-list__collection">
            <header>
                <a role="button" class="grow" href="/admin/collection/edit/{{$collection->url}}">
                    <b>{{$collection->name}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    janovic.media/collection/{{$collection->url}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    @foreach($collection->tags()->get() as $tag)
                        !{{$tag->name}}&nbsp;
                    @endforeach
                </a>
                <a role="button" class="delete-button" href="/admin/collection/delete?url={{$collection->url}}">✕</a>
            </header>
            <br>
            <span>
                {{$collection->description}}
            </span>
            <br>
            <br>
            <div style="padding: 0.33rem 0;">
                @foreach ($collection->photos()->get()->take(5) as $photo)
                    <img style="height: 3rem; width: 3rem; object-fit: cover;" src="{{$photo->path}}" alt="{{$photo->id}}">
                @endforeach
                @if($collection->photos()->get()->count() - 5 > 0)
                    &nbsp;&nbsp;+{{$collection->photos()->count() - 5}}&nbsp;&nbsp;
                @endif
            </div>
        </div>
        <br><br>
    @endforeach
</div>
