@extends($page_view, [ 'displaying_overlay' => true ])

<aside class="popup-menu">
    <a href="{{$href}}" class="popup-menu__background"></a>
    <section class="popup-menu__content">
        <div class="space"></div>
        @foreach($options as $key => $item)
            <a role="button" href="{{$item}}">{{$key}}</a>
        @endforeach
        <div class="space"></div>
        <a role="button" href="{{$href}}">cancel</a>
    </section>
</aside>
