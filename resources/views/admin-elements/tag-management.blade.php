<div class="tag-management">
    <header class="tag-management__header">
        <b>TAG MANAGEMENT</b>
        <div class="space"></div>
        <a role="button" href="/admin/tag/create">add a tag</a>
    </header>
    <br>
    <br>
    <div class="tag-container">
        @foreach($tags as $tag)
            <span class="tag">
                <a role="button" href="/admin/tag/edit?name={{$tag->name}}"><b>!{{$tag->name}}</b>&nbsp;&nbsp;(edit)</a>
                <a role="button" class="delete-button" href="/admin/tag/delete?name={{$tag->name}}">✕</a>
            </span>
        @endforeach
    </div>
    <br>
    <br>
    <br>
</div>
