@auth
    <section class="add-element" id="add-element{{$position}}">
        <span>+ add element:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <form
            method="POST"
            action="/edit/article/create?page_name={{$page_name}}&position={{$position}}#add-element{{$position}}"
            class="add-article"
        >
            @csrf
            <button type="submit">article</button>
        </form>
        <form
            method="POST"
            action="/edit/anchor/create?page_name={{$page_name}}&position={{$position}}#add-element{{$position}}"
            class="add-article"
        >
            @csrf
            <button type="submit">anchor</button>
        </form>
        <form
            method="POST"
            action="/edit/gallery/create?page_name={{$page_name}}&position={{$position}}#add-element{{$position}}"
            class="add-gallery"
        >
            @csrf
            <button type="submit">gallery</button>
        </form>
        <form
            method="POST"
            action="/edit/heading/create?page_name={{$page_name}}&position={{$position}}#add-element{{$position}}"
            class="add-heading"
        >
            @csrf
            <button type="submit">heading</button>
        </form>
    </section>
    <br><br><br>
@endauth
