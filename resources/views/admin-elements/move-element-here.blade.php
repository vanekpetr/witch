@auth
    @if($move_element_from_position != $position && $move_element_from_position+1 != $position)
        <section class="move-element-here">
            <form action="/edit/{{$type}}/move?from={{$move_element_from_position}}&to={{$position}}&page_name={{$page_name}}" method="POST">
                @csrf
                <button type="submit">move element here</button>
            </form>
        </section>
        <br><br><br>
    @else
        <section class="move-element-here move-element-here--disabled">
            : : :
        </section>
        <br><br><br>
    @endif
@endauth
