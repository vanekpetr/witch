@auth
    @if($move_service_from_position != $position && $move_service_from_position+1 != $position)
        <section class="move-service-here">
            <form action="/edit/service/move?from={{$move_service_from_position}}&to={{$position}}" method="POST">
                @csrf
                <button type="submit">move service element here</button>
            </form>
        </section>
        <br><br><br>
    @else
        <section class="move-service-here move-service-here--disabled">
            : : :
        </section>
        <br><br><br>
    @endif
@endauth
