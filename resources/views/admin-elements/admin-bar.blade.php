@auth
    <section class="admin-bar">
        <a role="button" href="/admin">admin page</a>
        <div class="space"></div>
        <div class="filler"></div>
        <span>
            (logged in as: {{ Auth::user()->username }})
        </span>
        <div class="space"></div>
        <a  role="button" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </section>
@endauth
